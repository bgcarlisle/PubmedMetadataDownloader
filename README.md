**NOTE: This repo is no longer being maintained, as its functions have been folded into the `pubmedtk` package: https://github.com/bgcarlisle/pubmedtk**

# PubmedMetadataDownloader

Downloads Pubmed metadata for a list of PMID's

# How to

This script expects to find a CSV in the same directory called `pmids.csv` with a column called `pmid`.

It will query Pubmed for each of those and print out a new timestamped CSV file that contains the languages, publication types and authors for the PMID in question.
